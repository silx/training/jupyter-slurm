# Jupyter-slurm.esrf.fr tutorial

This tutorial will give an overview of the https://jupyter-slurm.esrf.fr service which provides access to the ESRF slurm cluster remotely through the Jupyter interface (https://jupyter.org/).

Jupyter is a web application allowing to create and run documents (a.k.a. notebooks) containing code (Python, Octave,...), narrative text and visualization.

This tutorial is NOT a Python training, an "introduction to Python" training is provided as a separate event.

The tutorial will be delivered in English.

A tentative program:

## Part I: Introduction to Jupyter notebooks

- Running (Python) code
- Adding styled text: Markdown cells
- Notebook authoring

## Part II: A tour of jupyter

- Browse directories
- Start a shell terminal
- Run a Python notebook
- Run Octave code in a notebook: Octave notebook
- Use jupyterlab

## Part III: Using https://jupyter-slurm.esrf.fr

- Description of the different options provided by https://jupyter-slurm.esrf.fr
- Demo: Opening and running a Python notebook to:
  - Browse the filesystem
  - Browse inside HDF5 files
  - Perform some basic plots

## Part IV: Tips & Tricks

